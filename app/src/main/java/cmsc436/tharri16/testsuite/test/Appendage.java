package cmsc436.tharri16.testsuite.test;

/**
 * Helper to convey info about appendages being used in tests
 */

public enum Appendage {
    LEFT_HAND("Left Hand"),
    RIGHT_HAND("Right Hand"),
    LEFT_FOOT("Left Foot"),
    RIGHT_FOOT("Right Foot");

    private String s;
    Appendage (String s) {
        this.s = s;
    }

    @Override
    public String toString () {
        return s;
    }
}
