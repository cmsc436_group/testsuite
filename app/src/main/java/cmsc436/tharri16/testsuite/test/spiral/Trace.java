package cmsc436.tharri16.testsuite.test.spiral;

import android.graphics.Path;

import com.androidplot.xy.XYSeries;

import java.util.ArrayList;
import java.util.List;

import cmsc436.tharri16.csvlibrary.CSVHelper;
import cmsc436.tharri16.csvlibrary.CSVRow;

/**
 * Resembles a Path, stores x, y, t coordinates for now
 */

class Trace implements XYSeries {

    private int n = 0;
    private boolean inc = true;
    private float cx, cy;

    private String title = null;
    private List<Coord> coords;
    private List<Path> paths;
    private CSVHelper<Coord> csvHelper;

    // will auto transform screen coords to center coords
    Trace(float center_x, float center_y) {
        coords = new ArrayList<>();
        paths = new ArrayList<>();
        cx = center_x;
        cy = center_y;
        csvHelper = new CSVHelper<>(Coord.class);
    }

    Trace(float center_x, float center_y, String title) {
        this(center_x, center_y);
        this.title = title;
    }

    void add(float x, float y, long t) {
        Coord c = new Coord(x-cx, y-cy, t);
        coords.add(c);
        csvHelper.addRow(c);

        if (inc) {
            n += 1;
            inc = false;
            paths.add(new Path());
            paths.get(n-1).moveTo(x, y);
        } else {
            paths.get(n-1).lineTo(x, y);
        }
    }

    void addBreak() {
        coords.add(Coord.getBreak());
        csvHelper.addRow(Coord.getBreak());
        inc = true;
    }

    public List<Coord> getCoords () {
        return coords;
    }

    int getNTraces() {
        return n;
    }

    List<Path> getPaths() {
        return paths;
    }

    CSVHelper<Coord> getCSVHelper() {
        return csvHelper;
    }

    void clear() {
        coords.clear();
        paths.clear();
        n = 0;
        inc = true;
    }

    void setOrigin (float x, float y) {
        clear();
        cx = x;
        cy = y;
    }

    @Override
    public int size() {
        return coords.size();
    }

    @Override
    public Number getX(int index) {
        return coords.get(index).x;
    }

    @Override
    public Number getY(int index) {
        return coords.get(index).y;
    }

    @Override
    public String getTitle() {
        return title == null ? "" : title;
    }

    @SuppressWarnings("WeakerAccess")
    public static class Coord extends CSVRow {
        float x, y;
        long t;
        boolean is_break; // used for indicating breaks in data

        Coord (float x, float y, long t) {
            this.x = x;
            this.y = y;
            this.t = t;
            this.is_break = false;
        }

        Coord () {
            this.x = 0;
            this.y = 0;
            this.t = 0;
            is_break = false;
        }

        static Coord getBreak() {
            Coord c = new Coord(0, 0, 0);
            c.is_break = true;

            return c;
        }

        @Override
        public int getNumberOfColumns() {
            return 4;
        }

        @Override
        public String getNth(int i) {
            switch (i) {
                case 0:
                    return Integer.toString((int) x);
                case 1:
                    return Integer.toString((int) y);
                case 2:
                    return Long.toString(t);
                case 3:
                    return Boolean.toString(is_break);
                default:
                    return null;
            }
        }

        @Override
        public boolean setNth(int i, String s) {
            try {
                switch (i) {
                    case 0:
                        x = Integer.valueOf(s);
                        return true;
                    case 1:
                        y = Integer.valueOf(s);
                        return true;
                    case 2:
                        t = Long.valueOf(s);
                        return true;
                    case 3:
                        is_break = Boolean.valueOf(s);
                        return true;
                    default:
                        return false;
                }
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
    }

}
