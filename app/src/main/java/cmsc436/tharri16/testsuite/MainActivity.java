package cmsc436.tharri16.testsuite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import cmsc436.tharri16.testsuite.test.Appendage;
import cmsc436.tharri16.testsuite.test.Arg;
import cmsc436.tharri16.testsuite.test.CoordinatorActivity;
import cmsc436.tharri16.testsuite.test.spiral.TracerActivity;
import cmsc436.tharri16.testsuite.test.tap.TappingGame;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rv = (RecyclerView) findViewById(R.id.test_recyclerview);
        rv.post(new Runnable() {
            @Override
            public void run() {
                int w = rv.getWidth();
                int image_w = (int) getResources().getDimension(R.dimen.recyclerview_image_size);
                layoutManager.setSpanCount(w/((int) (image_w * 1.5)));
            }
        });

        layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return layoutManager.getSpanCount();
                }

                return 1;
            }
        });

        // TODO replace bundle arg with redirect through settings screen
        PracticeModeAdapter.Test[] tests = new PracticeModeAdapter.Test[] {
                new PracticeModeAdapter.Test(TracerActivity.class, "Spiral", R.drawable.ic_spiral, Arg.makeBundle(0, 0, Appendage.RIGHT_HAND)),
                new PracticeModeAdapter.Test(TappingGame.class, "Tap", R.drawable.ic_tap, Arg.makeBundle(0, 0, Appendage.RIGHT_HAND)),
                new PracticeModeAdapter.Test(IntroScreenActivity.class, "Intro 2", R.drawable.ic_bubble, null),
                new PracticeModeAdapter.Test(IntroScreenActivity.class, "Intro 3", R.drawable.ic_trending_up_black_24dp, null),
        };

        rv.setLayoutManager(layoutManager);
        rv.setAdapter(new PracticeModeAdapter(tests));
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.daily_tests:
                Intent i = new Intent(this, CoordinatorActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
