package cmsc436.tharri16.testsuite.test.spiral;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cmsc436.tharri16.testsuite.R;
import cmsc436.tharri16.testsuite.test.Appendage;
import cmsc436.tharri16.testsuite.test.Arg;
import cmsc436.tharri16.testsuite.test.CoordinatorActivity;
import cmsc436.tharri16.testsuite.test.Result;

/**
 * Fullscreen, and holds a custom view for tracing
 */

public class TracerActivity extends AppCompatActivity {

    private final int permissionsRequestCode = 436;
    private TracerView tv;

    public static CoordinatorActivity.AppendageChecker getAppendageChecker () {
        return new CoordinatorActivity.AppendageChecker() {
            @Override
            public boolean supportsAppendage(Appendage appendage) {
                return appendage == Appendage.RIGHT_HAND || appendage == Appendage.LEFT_HAND;
            }
        };
    }

    @Override
    public void onCreate (final Bundle savedInstance) {
        super.onCreate(savedInstance);
        onWindowFocusChanged(true);
        setContentView(R.layout.activity_tracer);

        Appendage appendage = Arg.getAppendage(getIntent().getExtras());
        int trial = Arg.getTrial(getIntent().getExtras());
        int trial_out_of = Arg.getTrialOutOf(getIntent().getExtras());

        ((TextView) findViewById(R.id.appendage_textview)).setText(appendage.toString());

        String message = trial > 0 && trial_out_of > 0 ? "Trial " + trial + " / " + trial_out_of : "Practice Mode";
        ((TextView) findViewById(R.id.trial_count_textview)).setText(message);

        tv = (TracerView) findViewById(R.id.tracer_view);
        tv.setOnFinishListener(new Runnable() {
            @Override
            public void run() {
                done();
            }
        });
        tv.setTraceMode(appendage == Appendage.RIGHT_HAND ? TracerView.TRACE_CW : TracerView.TRACE_CCW);
    }

    private void done() {
        try {
            File f = saveTracerView();
            Intent i = new Intent();
            i.putExtras(Result.makeBundle(f, 0f)); // TODO real data point
            setResult(RESULT_OK, i);
            finish();
        } catch (PermissionException e) {
            // nothing
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode,
                                            @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == this.permissionsRequestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                done();
            }
        }
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus) {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }

    private class PermissionException extends Exception {}

    private File saveTracerView() throws PermissionException {

        if (ContextCompat.checkSelfPermission(TracerActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(TracerActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    permissionsRequestCode);
            throw new PermissionException();
        }

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // External media not mounted, so nothing to write
            return null;
        }

        Bitmap bm = Bitmap.createBitmap(tv.getWidth(), tv.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        tv.onSave(c);

        String dateTime = getDateTime();
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "SpiralTrial_"+dateTime);
        if (!dir.mkdirs() && !dir.exists()) {
            // error when creating directory
            return null;
        }
        File imageFile = new File(dir, getString(R.string.spiral_image_filename));
        File userCSV = new File(dir, getString(R.string.spiral_user_data_filename));
        File predrawnCSV = new File(dir, getString(R.string.spiral_predrawn_data_filename));
        File properties = new File(dir, getString(R.string.spiral_metadata_filename));

        try {
            FileOutputStream out = new FileOutputStream(imageFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            File temp_user = tv.getUserTrace().getCSVHelper().write(this);
            if (tv.getPredrawn().getNTraces() > 0) {
                File temp_predrawn = tv.getPredrawn().getCSVHelper().write(this);
                copyFile(temp_predrawn, predrawnCSV, "Error while writing predrawn CSV");
            }
            copyFile(temp_user, userCSV, "Error while writing user CSV");
            writeProperties(properties);

            Toast.makeText(this, "Saved " + dateTime, Toast.LENGTH_SHORT).show();
            return dir;
        } catch (IOException e) {
            Log.e(this.getClass().toString(), e.getMessage());
            return null;
        }
    }

    private void writeProperties (File f) throws FileNotFoundException {
        // TODO actually get user info

        FileOutputStream fileOutputStream = new FileOutputStream(f);
        PrintStream printStream = new PrintStream(fileOutputStream);
        printStream.println("mode="+tv.getTraceMode()); // TODO translate to human readable
        printStream.println("ntraces="+tv.getUserTrace().getNTraces());
    }

    private String getDateTime () {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    private void copyFile (File src, File dest, String error_message) throws IOException {

        if (src == null || dest == null) {
            throw new IOException(error_message);
        }

        FileChannel in = new FileInputStream(src).getChannel();
        FileChannel out = new FileOutputStream(dest).getChannel();

        try {
            in.transferTo(0, in.size(), out);
        } finally {
            if (in != null) {
                in.close();
            }

            out.close();
        }
    }
}
