package cmsc436.tharri16.testsuite.test;

import android.os.Bundle;

import java.io.File;

/**
 * Helper class for returning raw data, and a processed value for google sheets
 */

public enum Result {
    FILENAME("filename"),
    DATA_POINT("data_point");

    private String s;
    Result(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return s;
    }

    public static Bundle makeBundle(File raw_data, float data_point) {
        Bundle b = new Bundle();
        if (raw_data != null) {
            b.putString(FILENAME.toString(), raw_data.getAbsolutePath());
        }
        b.putFloat(DATA_POINT.toString(), data_point);
        return b;
    }

    public static File getFile (Bundle b) {
        String name = b.getString(FILENAME.toString());
        if (name == null) {
            return null;
        }

        File f = new File(name);
        if (!f.exists()) {
            return null;
        }

        return f;
    }

    public static float getDataPoint (Bundle b) {
        return b.getFloat(DATA_POINT.toString(), 0f);
    }
}
