package cmsc436.tharri16.testsuite;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * A small subclass to make square test views
 */

public class SquareLayout extends FrameLayout {


    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
