package cmsc436.tharri16.testsuite.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import cmsc436.tharri16.testsuite.R;
import cmsc436.tharri16.testsuite.test.spiral.TracerActivity;
import cmsc436.tharri16.testsuite.test.tap.TappingGame;

public class CoordinatorActivity extends AppCompatActivity {

    private final int REQUEST_CODE = 436;
    private List<Intent> intents;
    private int current_intent = 0;
    private final Class<?>[] classes = new Class[] {
            TracerActivity.class,
            TappingGame.class,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onWindowFocusChanged(true);
        setContentView(R.layout.activity_coordinator);

        createTests();
        startActivityForResult(intents.get(current_intent), REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE + current_intent && resultCode == RESULT_OK) {
            // TODO results handling
            if (data.getExtras() != null) {
                File f = Result.getFile(data.getExtras());
                float data_point = Result.getDataPoint(data.getExtras());

                Log.i(getClass().getSimpleName(), "File: " + (f == null ? "NULL" : f.getAbsolutePath()) + " Data: " + data_point);
            }

            current_intent += 1;
            if (current_intent >= intents.size()) {
                finish();
                return;
            }
            startActivityForResult(intents.get(current_intent), REQUEST_CODE + current_intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus) {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }

    private void createTests() {
        AppendageChecker[] appendageCheckers = new AppendageChecker[classes.length];

        // oh no
        for (int i = 0; i < classes.length; i++) {
            try {
                @SuppressWarnings("unchecked")
                Method m = classes[i].getMethod("getAppendageChecker");  // I swear it's okay
                appendageCheckers[i] = (AppendageChecker) m.invoke(null);
            } catch (NoSuchMethodException e) {
                Log.e(getClass().getSimpleName(), "Mandatory public static method getAppendageChecker not found on: " + classes[i].getName());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                Log.e(getClass().getSimpleName(), classes[i].getName() + "#getAppendageChecker() not accessible");
            }
        }

        intents = new ArrayList<>();
        for (int i = 0; i < appendageCheckers.length; i++) {
            AppendageChecker ac = appendageCheckers[i];
            for (Appendage a : Appendage.values()) {
                if (ac.supportsAppendage(a)) {
                    // TODO PARAMETERIZE BY SETTINGS
                    int trial_out_of = 3;
                    for (int trial = 1; trial <= trial_out_of; trial++) {
                        Intent launcher = new Intent(this, classes[i]);
                        launcher.putExtras(Arg.makeBundle(trial, trial_out_of, a));
                        intents.add(launcher);
                    }
                }
            }
        }
    }

    public interface AppendageChecker {
        boolean supportsAppendage (Appendage appendage);
    }
}
