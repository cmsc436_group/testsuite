package cmsc436.tharri16.testsuite.test;

import android.os.Bundle;

/**
 * Helper class for passing info to tests
 */

public enum Arg {

    TRIAL_NUM("trial num"),
    TRIAL_OUT_OF("out of"),
    APPENDAGE("appendage");

    private String s;
    Arg(String s) {
        this.s = s;
    }

    @Override
    public String toString () {
        return s;
    }

    public static Bundle makeBundle (int trial, int out_of, Appendage appendage) {
        Bundle b = new Bundle();
        b.putInt(TRIAL_NUM.toString(), trial);
        b.putInt(TRIAL_OUT_OF.toString(), out_of);
        b.putInt(APPENDAGE.toString(), appendage.ordinal());
        return b;
    }

    public static int getTrial(Bundle b) {
        if (b == null) {
            return 0;
        }

        return b.getInt(TRIAL_NUM.toString());
    }

    public static int getTrialOutOf(Bundle b) {
        if (b == null) {
            return 0;
        }

        return b.getInt(TRIAL_OUT_OF.toString());
    }

    public static Appendage getAppendage(Bundle b) {
        if (b == null) {
            return Appendage.RIGHT_HAND;
        }

        int a = b.getInt(APPENDAGE.toString(), 0);
        return Appendage.values()[a % Appendage.values().length];
    }
}
