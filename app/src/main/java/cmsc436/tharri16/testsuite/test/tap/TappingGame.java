package cmsc436.tharri16.testsuite.test.tap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import cmsc436.tharri16.testsuite.R;
import cmsc436.tharri16.testsuite.test.Appendage;
import cmsc436.tharri16.testsuite.test.Arg;
import cmsc436.tharri16.testsuite.test.CoordinatorActivity;
import cmsc436.tharri16.testsuite.test.Result;

/**
 * A class to perform the tapping game
 *
 * Will eventually return a filename to a csv
 * of tap times
 */

public class TappingGame extends AppCompatActivity {

    private boolean practice_mode = false;
    private TextView secondsRemaining;
    private Button button;
    private TimerTask startTask;
    private TimerTask countdownTask;
    private boolean in_game = false;
    private int taps;

    public static CoordinatorActivity.AppendageChecker getAppendageChecker () {
        return new CoordinatorActivity.AppendageChecker() {
            @Override
            public boolean supportsAppendage(Appendage appendage) {
                return true;
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onWindowFocusChanged(true);
        setContentView(R.layout.activity_tapping_game);

        Bundle extras = getIntent().getExtras();

        Appendage appendage = Arg.getAppendage(extras);
        int trial = Arg.getTrial(extras);
        int trial_out_of = Arg.getTrialOutOf(extras);

        practice_mode = trial == trial_out_of && trial_out_of == 0;

        ((TextView) findViewById(R.id.trial_count_textview)).setText(practice_mode ?
            getString(R.string.practice_mode_text) :
            "Trial " + trial + " / " + trial_out_of);

        ((TextView) findViewById(R.id.appendage_textview)).setText(appendage.toString());
        secondsRemaining = (TextView) findViewById(R.id.time_remaining_textview);
        setTimeRemaining(-1);

        button = (Button) findViewById(R.id.tap_button);

        findViewById(R.id.tap_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (in_game) {
                    taps += 1;
                } else {
                    button.setEnabled(false);
                    startTask = new TimerTask(5, new TimerTask.TimerProgressRunnable() {
                        @Override
                        public void onProgressPublished(int i) {
                            setButton(i);
                        }

                        @Override
                        public void onFinished() {
                            setButton(getString(R.string.tap_button_text));
                            in_game = true;
                            button.setEnabled(true);
                            countdownTask = new TimerTask(10, new TimerTask.TimerProgressRunnable() {
                                @Override
                                public void onProgressPublished(int i) {
                                    setTimeRemaining(i);
                                }

                                @Override
                                public void onFinished() {
                                    done();
                                    in_game = false;
                                }
                            });
                            countdownTask.execute();
                        }
                    });
                    startTask.execute();
                }
            }
        });
    }

    private void done() {
        setTimeRemaining(0);
        AlertDialog ad = new AlertDialog.Builder(this)
                .setPositiveButton("OKAY", null)
                .setMessage(getString(R.string.tap_report_text, taps))
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (!practice_mode) {
                            // TODO practice mode no results
                            // setResult();
                        }

                        Intent i = new Intent();
                        i.putExtras(Result.makeBundle(null, taps)); // TODO raw data
                        setResult(RESULT_OK, i);
                        finish();
                    }
                })
                .create();

        // hacks to keep in immersive mode 9/10 would hack again
        if (ad.getWindow() != null) {
            ad.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }
        ad.setCanceledOnTouchOutside(false);
        ad.show();
        if (ad.getWindow() != null) {
            ad.getWindow().getDecorView().setSystemUiVisibility(getWindow().getDecorView().getSystemUiVisibility());
            ad.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }
    }

    private void setTimeRemaining (int i) {
        secondsRemaining.setText(i < 0 ? "" : getString(R.string.tap_seconds_remaining_text, i));
    }

    private void setButton (int i) {
        button.setText(getString(R.string.tap_button_countdown_text, i));
    }

    private void setButton (String s) {
        button.setText(s);
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus) {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (startTask != null) {
            startTask.cancel(true);
        }

        if (countdownTask != null) {
            countdownTask.cancel(true);
        }
    }

    private static class TimerTask extends AsyncTask<Void, Integer, Void> {

        TimerProgressRunnable progressRunnable;
        int time;

        TimerTask (int time, TimerProgressRunnable progressRunnable) {
            this.progressRunnable = progressRunnable;
            this.time = time;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = time; i > 0; i--) {
                publishProgress(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    return null;
                }
                if (isCancelled()) {
                    return null;
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (progressRunnable != null) {
                progressRunnable.onProgressPublished(values[0]);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (progressRunnable != null) {
                progressRunnable.onFinished();
            }
        }

        interface TimerProgressRunnable {
            void onProgressPublished (int i);
            void onFinished ();
        }
    }
}
