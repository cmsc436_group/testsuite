package cmsc436.tharri16.testsuite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A class to adapt a list of Test classes to the recyclerview
 */

class PracticeModeAdapter extends RecyclerView.Adapter<PracticeModeAdapter.MyViewHolder> {

    private Test[] tests;

    private static final int VIEWTYPE_HEADER = 0;
    private static final int VIEWTYPE_ITEM = 1;


    PracticeModeAdapter(Test[] tests) {
        this.tests = tests;
    }

    @Override
    public int getItemViewType (int position) {
        return position == 0 ? VIEWTYPE_HEADER : VIEWTYPE_ITEM;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int resource = viewType == VIEWTYPE_HEADER ?
                R.layout.test_recyclerview_header : R.layout.test_recyclerview_item;

        FrameLayout layout = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(resource, parent, false);

        return new MyViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (position == 0) {
            holder.label.setText(R.string.test_recyclerview_header);
            return;
        }

        if (holder.icon != null) {
            holder.icon.setImageResource(tests[position-1].image_resource);
        }
        holder.label.setText(tests[position-1].label);
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(holder.root.getContext(), tests[holder.getAdapterPosition()-1].cls);
                Bundle b = tests[holder.getAdapterPosition()-1].args;
                if (b != null) {
                    i.putExtras(b);
                }
                holder.root.getContext().startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return tests.length+1;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        FrameLayout root;
        TextView label;
        ImageView icon;

        MyViewHolder(FrameLayout itemView) {
            super(itemView);

            root = itemView;
            label = (TextView) itemView.findViewById(R.id.item_text_view); // is in both layouts
            icon = (ImageView) itemView.findViewById(R.id.item_image_view); // NOT in both
        }
    }

    static class Test {
        Class<?> cls;
        String label;
        int image_resource;
        Bundle args;

        Test (Class<?> cls, String label, int image_resource, @Nullable Bundle args) {
            this.cls = cls;
            this.label = label;
            this.image_resource = image_resource;
            this.args = args;
        }
    }
}
